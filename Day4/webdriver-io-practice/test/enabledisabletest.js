describe("enable and disable test", function() {

    it("wait for enabled", function() {

        browser.url("https://the-internet.herokuapp.com/dynamic_controls");
        browser.maximizeWindow();

        const textBox = $('#input-example > input[type=text]');
        const button = $('#input-example > button');

        button.click();
        textBox.waitForEnabled();

        browser.pause(2000);

    });

    // learn about browser.waitUnit()

});