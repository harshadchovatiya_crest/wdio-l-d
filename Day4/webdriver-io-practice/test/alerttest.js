describe("handle alert", function() {

    it("accept alert", function() {

        browser.url("https://the-internet.herokuapp.com/javascript_alerts");
        
        const firstAlert = $('ul li:nth-child(1) button');
        firstAlert.waitForDisplayed();
        firstAlert.click();

        browser.pause(3000);

        const firstAlertText = browser.getAlertText();
        console.log(firstAlertText);

        browser.acceptAlert();

        browser.pause(3000);

    });

    it.skip("dismiss alert", function() {

        const secondAlert = $('ul li:nth-child(2) button');
        secondAlert.waitForDisplayed();
        secondAlert.click();

        browser.pause(3000);

        const secondAlertText = browser.getAlertText();
        console.log(secondAlertText);

        browser.dismissAlert();

        browser.pause(3000);

    });


    it("write on alert box and accept", function() {

        const thirdAlert = $('ul li:nth-child(3) button');
        thirdAlert.waitForDisplayed();
        thirdAlert.click();

        browser.pause(3000);

        browser.sendAlertText("Sending alert text...");
        browser.pause(2000);

        browser.acceptAlert();
        browser.pause(3000);

    });


    // waitForEnabled(milliseconds) //waiting until element to be enabled till the time passed in argument
});