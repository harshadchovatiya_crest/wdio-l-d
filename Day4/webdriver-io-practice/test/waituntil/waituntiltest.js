describe("title of page must be displayed in a spicific time", function() {

    it("is title displayed ?", function() {

        browser.url("https://the-internet.herokuapp.com/");

        browser.waitUntil(function() {
            const header = $('h1')

            return browser.getTitle() === "The Internet"  && header.getText() === "Welcome to the-internet";
        }, 5000, "Title is not displayed within 5 second");

    });

});