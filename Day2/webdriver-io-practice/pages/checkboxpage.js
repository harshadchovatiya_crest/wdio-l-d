class CheckBoxPage {

    getLink(index) {
        return $(`ul li:nth-child(${index}) a`);
    }

    clickOnLink(index) {
        const link = this.getLink(index);
        link.waitForDisplayed();
        link.click();
    }

    getCheckBox(index) {
        return $(`form input:nth-child(${index})`);
    }

    clickOnCheckBox(index) {
        const checkbox = this.getCheckBox(index);
        checkbox.waitForDisplayed();
        checkbox.click();
    }
}

module.exports = new CheckBoxPage();