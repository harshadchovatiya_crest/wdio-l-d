class BlazeMeter {

    get mainHeader() {
        return $('#main_c_container > div > section.home-features > div.home-features_button > a');
    }

    get parent() {
        return $('ul.list-nav-links');
    }

    get childElements() {
        return this.parent.$$('li');
    }

    get textForLi() {
        return this.childElements.filter(element => {
            console.log(element.getText());
        });
    }

    specificChildElement(index) {
        return this.parent.$(`li:nth-child(${index})`); // 1 base index
    }

    specificChildElementText(index) {
        return this.specificChildElement(index).getText();
    }

    get productLink() {
        return $('ul.list-nav-links li:nth-child(1) a');
    }

    clickOnProductLink() {
        if(this.productLink.isDisplayed() === true) {
            this.productLink.click();
        }
    }
}

module.exports = new BlazeMeter();