class LoginPage {

    get userNameTextBox() {
        return $('#userName')
    }

    get passwordTextBox() {
        return $('#password')
    }

    get loginButton() {
        return $('#login')
    }

}

module.exports = new LoginPage();