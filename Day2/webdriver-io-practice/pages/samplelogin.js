class SampleLogin {

    get userName() {
        return $('#username');
    }

    get password() {
        return $('#password');
    }

    get loginButton() {
        return $('button');
    }

    enterUserName(name) {
        const usernameField = this.userName;
        usernameField.waitForDisplayed();
        usernameField.setValue(name);
    }

    enterPassword(password) {
        const passwordField = this.password;
        passwordField.waitForDisplayed();
        passwordField.setValue(password);
    }

    clickOnLoginButton() {
        const loginButton = this.loginButton;
        loginButton.waitForDisplayed();
        loginButton.click();
    }
    
}

module.exports = new SampleLogin();