const blazeMeter = require('../pages/blazemeter');

describe("Testing double dollar", function() {

    it("First test", function() {

        browser.url('https://www.blazemeter.com/');
        
        // browser.setWindowSize(1366, 784);
        // let answer = blazeMeter.textForLi;
        // console.log(answer);

        let thirdElement = blazeMeter.specificChildElement(3);
        console.log(thirdElement.getText());
    });

    it("testing with non getter", function() {

        console.log(blazeMeter.specificChildElementText(4));
        browser.pause(4000);

    });

    it("learning isDisplayed", function() {

        console.log(blazeMeter.mainHeader.isDisplayed());

    });

    it("learning isEnabled", function() {

        console.log(blazeMeter.mainHeader.isEnabled());

    });

    it("learning isExisting", function() {

        console.log(blazeMeter.mainHeader.isExisting());

    });

    it("learning isDisplayedInViewport", function() {

        console.log(blazeMeter.mainHeader.isDisplayedInViewport());

    });

    it("click on the link after it display", function() {

        blazeMeter.clickOnProductLink();
        browser.pause(5000);

    });

});
