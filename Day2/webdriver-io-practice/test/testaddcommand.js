describe("Test browser add command", function() {

    it("testing first time", function() {
        browser.url("https://www.amazon.com/");
    
        browser.addCommand('getUrlAndTitle', function (customParam) {
            // `this` refers to the `browser` scope
            return {
                url: this.getUrl(),
                title: this.getTitle(),
                customParam: customParam
            }
        });
        
        const result = browser.getUrlAndTitle('foobar');
        assert.strictEqual(result.url, 'https://www.amazon.com/')
    });

});