const checkBoxPage = require('../pages/checkboxpage');

describe("Testing checkbox", function() {

    it("click on link", function() {
        browser.url("https://the-internet.herokuapp.com/");

        expect(browser.getUrl()).equals("https://the-internet.herokuapp.com/");
        checkBoxPage.clickOnLink(6);

    });

    it("click on the checkbox", function() {

        checkBoxPage.clickOnCheckBox(1);
        expect(checkBoxPage.getCheckBox(1).isSelected()).equals(true);
        browser.pause(4000);

    });

});