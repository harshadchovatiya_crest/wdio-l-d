const loginPage = require('../pages/loginpage')

describe("Starting with page object model", function() {

    it("get header value", function() {
        browser.url("/login");
        loginPage.userNameTextBox.setValue("JohnDoe");
        loginPage.passwordTextBox.setValue("JohnDoe123@");
        loginPage.loginButton.click();
        browser.pause(3000);
    });

});