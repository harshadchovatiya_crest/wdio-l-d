const sampleLogin = require('../pages/samplelogin');

describe("Testing the login page", function() {

    it("navigate to login page", function() {

        browser.url("https://the-internet.herokuapp.com/login");

    });

    it("enter login credentials", function() {

        sampleLogin.enterUserName("tomsmith");
        sampleLogin.enterPassword("SuperSecretPassword!");

        assert.equal("tomsmith", sampleLogin.userName.getValue());
        sampleLogin.userName.clearValue();
        browser.pause(3000);

    });

    it("click on login", function() {

        sampleLogin.clickOnLoginButton();
        browser.pause(3000);

    });

});