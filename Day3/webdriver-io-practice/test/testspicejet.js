const spiceJet = require('../pages/spicejet');

describe("Test moveTo", function() {

    // to skip test case write it.skip
    it.skip("login member", function() {

        browser.url("https://www.spicejet.com/");
        spiceJet.moveToElement(spiceJet.loginSignupLink);
        browser.pause(2000);

        spiceJet.moveToElement(spiceJet.spiceClubMem);
        browser.pause(2000);

        spiceJet.clickOnElement(spiceJet.memberLogin);
        browser.pause(3000);

    });

    it("testing tab key presses", function() {

        browser.url("https://the-internet.herokuapp.com/key_presses");
        
        spiceJet.enterText("Tab");

        const text = spiceJet.resultTagText;
        assert.equal(text, "You entered: TAB");
        
        browser.pause(2000);

    });

    it("testing shift key presses", function() {

        spiceJet.enterText("Shift");

        const text = spiceJet.resultTagText;
        assert.equal(text, "You entered: SHIFT");
        
        browser.pause(2000);

    });

});