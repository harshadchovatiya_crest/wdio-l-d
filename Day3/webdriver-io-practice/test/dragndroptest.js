describe("drag and drop feature", function() {

    it("do drag and drop", function() {

        browser.url("https://jqueryui.com/resources/demos/droppable/default.html");

        browser.pause(3000);

        const sourceEle = $('#draggable');
        const destinationEle = $('#droppable')

        sourceEle.dragAndDrop(destinationEle);

        browser.pause(3000);

    });

});