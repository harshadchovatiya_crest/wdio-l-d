const spiceJet = require('../pages/spicejet');

describe("testing dropdown menu", function() {

    it("click on drop down", function() {

        browser.url("https://www.spicejet.com/");

        const passengerDropDown = spiceJet.passengersDropDown;
        spiceJet.clickOnElement(passengerDropDown);
        browser.pause(3000);

        const adultDropDown = spiceJet.adultDropDown;
        spiceJet.clickOnElement(adultDropDown);
        adultDropDown.selectByVisibleText('2');  // first method to select from dropdown

        const childDropDown = spiceJet.childDropDown;
        spiceJet.clickOnElement(childDropDown);
        childDropDown.selectByIndex(2);  // second method to select from dropdown

        const infantDropDown = spiceJet.infantDropDown;
        spiceJet.clickOnElement(infantDropDown);
        infantDropDown.selectByAttribute('value', '1');  // third method to select from dropdown

        spiceJet.clickOnElement(passengerDropDown);
        browser.pause(3000);

    });

    // it("get all value from dropdown", function() {

    //     browser.url("https://www.spicejet.com/");
        

    // });

});
