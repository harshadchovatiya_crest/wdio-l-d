describe("testcases for scrolling to element", function() {

    it("scroll to element", function() {
        
        browser.url("https://www.spicejet.com/");
        
        const link = $('=March 2020');

        console.log("before scrolling in viewport : ", link.isDisplayedInViewport());
        link.scrollIntoView();
        console.log("after scrolling in viewport : ", link.isDisplayedInViewport());
        browser.pause(4000);

    });

});