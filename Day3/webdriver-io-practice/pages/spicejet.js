class SpiceJet {

    get loginSignupLink() {
        return $('#ctl00_HyperLinkLogin');
    }

    get spiceClubMem() {
        return $('=SpiceClub Members');
    }

    get memberLogin() {
        return $('=Member Login');
    }

    moveToElement(element) {
        element.waitForDisplayed();
        element.moveTo();
    }

    clickOnElement(element) {
        element.waitForDisplayed();
        element.click();
    }

    get searchBar() {
        return $('#target');
    }

    get resultTag() {
        return $('#result');
    }

    get resultTagText() {
        const resultTag = this.resultTag;
        resultTag.waitForDisplayed();
        return resultTag.getText();
    }

    enterText(text) {
        const searchBar = this.searchBar;
        searchBar.waitForDisplayed();
        searchBar.setValue(text);
    }

    get passengersDropDown() {
        return $("#divpaxinfo");
    }

    get adultDropDown() {
        return $('#ctl00_mainContent_ddl_Adult');
    }

    get childDropDown() {
        return $('#ctl00_mainContent_ddl_Child');
    }

    get infantDropDown() {
        return $('#ctl00_mainContent_ddl_Infant');
    }
    
}

module.exports = new SpiceJet();