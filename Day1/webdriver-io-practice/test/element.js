describe("Interaction with web element", function() {

    it("enter value in a field", function() {
        browser.url('/');
        const search = $('#twotabsearchtextbox');
        search.setValue('Iphone SE');
        browser.pause(5000);
    });

    it("get value from element", function(){
        const text = $('span.a-size-base.a-color-base');
        let data = text.getText();
        console.log(data);
    });

    it("click on the element", function() {
        const serachButton = $('#nav-search-submit-button');
        serachButton.click();
    });
});