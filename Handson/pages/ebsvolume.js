const elementUtil = require('../util/elementutil');
const CONSTANT = require('../constant');


class EBSVolume {

    get accountIDField() {
        return $('//*[@aria-label="Account ID"]');
    }

    get regionField() {
        return $('//*[@aria-label="Regions"]');
    }

    get selectedButtonOfAccountField() {
        return $$('//div[@id="input1"]//*[@data-test="selected-option"]');
    }

    get AccountIDSFromDropDown() {
        return $$('//button[@data-selectable="false"]');
    }

    get selectedButtonOfRegionField() {
        return $$('//div[@id="input2"]//*[@data-test="selected-option"]');
    }

    get regionsFromDropDown() {
        return $$('//button[@data-selectable="false"]');
    }

    get inUseEBSVolumes() {
        return $('//div[@id="panel1"]//*[@class="single-result"]');
    }

    get inUseEBSVloumesPanel() {
        return $('//div[@id="panel1"]//h3');
    }

    get inUseEBSVloumeSizePanel() {
        return $('//div[@id="panel2"]//h3');
    }

    get inUseEBSVolumesTotalOf() {
        return $('//div[@id="panel1"]//div[@class="single-total"]');
    }

    get inUseEBSVolumeSizeTotalOf() {
        return $('//div[@id="panel2"]//div[@class="single-total"]');
    }

    get inUseEBSVolumeSize() {
        return $('//div[@id="panel2"]//*[@class="single-result"]');
    }

    get alertSymbol() {
        return $$('//*[@class="icon-alert"]');
    }

    get warningSymbol() {
        return $$('//i[@class="icon-warning-sign"]');
    }

    clickOnAccountIDField(){

        const accountIDFieldEle = this.accountIDField;
        browser.waitUntil(function() {
            return accountIDFieldEle.isDisplayed() === true;
        }, {timeout:CONSTANT.GENERAL_WAIT});
        elementUtil.clickOnElement(accountIDFieldEle);
    
    }

    clearValueOfAccountIDField() {

        const selectedButton = this.selectedButtonOfAccountField;
        for(var i=0; i<selectedButton.length; i++) {
            elementUtil.clickOnElement(selectedButton[i]);
        }
    
    }

    selectAccountIDUsingIndex(index, remaining_ids) {
        let accountIDS;
        const self = this;
        browser.waitUntil(function() {
            accountIDS = self.AccountIDSFromDropDown;
            return accountIDS.length === remaining_ids;
        }, {timeout: CONSTANT.ACCOUNTID_WAIT})

        elementUtil.clickOnElement(accountIDS[index]);
    }

    clickOnPanel(panelname) {
        const self = this;
        if(panelname === "in-use-ebs-volumes") {
            let inUseEBS; 
            browser.waitUntil(function() {
                inUseEBS = self.inUseEBSVolumes;
                return inUseEBS.isDisplayed() === true;
            },{timeout: CONSTANT.PANEL_DATA_WAIT});

            assert.equal(inUseEBS.isClickable(), true);
            elementUtil.clickOnElement(inUseEBS);
        }

        if(panelname === "in-use-ebs-volume-size") {
            let inUseEBSSize; 
            browser.waitUntil(function() {
                inUseEBSSize = self.inUseEBSVolumeSize;
                return inUseEBSSize.isDisplayed() === true;
            },{timeout: CONSTANT.PANEL_DATA_WAIT});

            assert.equal(inUseEBSSize.isClickable(), true);
            elementUtil.clickOnElement(inUseEBSSize);
        }
    }

    clickOnRegionField() {

        const regionFieldEle = this.regionField;
        browser.waitUntil(function() {
            return regionFieldEle.isDisplayed() === true;
        }, {timeout: CONSTANT.GENERAL_WAIT});

        elementUtil.clickOnElement(regionFieldEle);
    }

    clearValueOfRegionField() {

        const selectedRegions = this.selectedButtonOfRegionField;

        browser.waitUntil(function() {
            return selectedRegions.length > 0;
        }, {timeout: CONSTANT.GENERAL_WAIT});

        for(var i=0; i<selectedRegions.length; i++) {
            elementUtil.clickOnElement(selectedRegions[i]);
        }
    }

    selectRegionUsingIndex(index){
        const self = this;

        let allRegions;
        browser.waitUntil(function() {
            allRegions = self.regionsFromDropDown;
            return allRegions.length > 0;
        }, {timeout: CONSTANT.REGION_WAIT});

        elementUtil.clickOnElement(allRegions[index]);
    
    }

    checkPresenceOfInUseEBSVolumesPanel() {

        const self = this;
        let inUseEBSVloumesPanelHeader; 
        browser.waitUntil(function() {
            inUseEBSVloumesPanelHeader = self.inUseEBSVloumesPanel;
            return inUseEBSVloumesPanelHeader.isDisplayed() === true
        }, {timeout: CONSTANT.PANEL_DISPLAY_WAIT});

        const firstPanel = inUseEBSVloumesPanelHeader;
        assert.equal(firstPanel.getText(), 'In-Use EBS Volumes');
   
    }

    checkPresenceOfInUseEBSVolumeSizePanel() {

        const self = this;
        let inUseEBSVloumeSizePanelHeader; 
        browser.waitUntil(function() {
            inUseEBSVloumeSizePanelHeader = self.inUseEBSVloumeSizePanel;
            return inUseEBSVloumeSizePanelHeader.isDisplayed() === true
        }, {timeout: CONSTANT.PANEL_DISPLAY_WAIT});

        const firstPanel = inUseEBSVloumeSizePanelHeader;
        assert.equal(firstPanel.getText(), 'In-Use EBS Volume Size');
    
    }

    checkForAlertSymbolInPanel(panelname) {

        let xpathStr;
        
        if(panelname === "in-use-ebs-volumes")
            xpathStr = '//div[@id="panel1"]//*[@class="icon-alert"]' 
        
        if(panelname === "in-use-ebs-volume-size")
            xpathStr = '//div[@id="panel2"]//*[@class="icon-alert"]' 

        browser.waitUntil(function() {
            return $(xpathStr).isDisplayed() === false;
        }, {timeout: CONSTANT.ALERT_WAIT});
    }


    checkForWarningSymbolInPanel(panelname) {
        let xpathStr;
        
        if(panelname === "in-use-ebs-volumes")
            xpathStr = '//div[@id="panel7"]//i[@class="icon-warning-sign"]' 
        
        if(panelname === "in-use-ebs-volume-size")
            xpathStr = '//div[@id="panel8"]//i[@class="icon-warning-sign"]' 

        browser.waitUntil(function() {
            return $(xpathStr).isDisplayed() === false;
        }, {timeout: CONSTANT.ALERT_WAIT});
    }

}

module.exports = new EBSVolume();