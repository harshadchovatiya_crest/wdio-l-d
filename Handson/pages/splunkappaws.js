const elementUtil = require('../util/elementutil');
const CONSTANT = require('../constant');

class SplunkAppAws {

    get usageDropDown() {
        return $('//a[@title="Usage"]');
    }

    get ebsVolume() {
        return $('//a[text()="EBS Volumes"]');
    }

    clickOnEBSVolumes() {
        let usageDropDownEle; 
        const self = this;
        browser.waitUntil(function() {
            usageDropDownEle = self.usageDropDown;
            return usageDropDownEle.isDisplayed() === true;
        }, {timeout: CONSTANT.GENERAL_WAIT});

        elementUtil.clickOnElement(usageDropDownEle);

        const ebsVolumeEle = this.ebsVolume;
        elementUtil.clickOnElement(ebsVolumeEle);

    }
}

module.exports = new SplunkAppAws();