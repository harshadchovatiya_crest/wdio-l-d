const CONSTANT = require('../constant');


class DisplayData {

    get spanOfRegionValue() {
        return $$('//*[@class="ace_quoted"]');
    }

    getSpansOfSelectedValues() {
        const self = this;
        let spans;
    
        browser.waitUntil(function() {
            spans = self.spanOfRegionValue;
            return spans.length > 1;
        }, {timeout: CONSTANT.GENERAL_WAIT});
    
        return spans;
    }

}

module.exports = new DisplayData();