const elementUtil = require('../util/elementutil');
const CONSTANT = require('../constant');

class HomePage {

    get splunkAppForAWS() {
        return $('//div[@data-appid="splunk_app_aws"]');
    }

    navigateToSplunkAppForAWS() {
        const splunkAppForAWSEle = this.splunkAppForAWS;
       
        browser.waitUntil(function() {
            return splunkAppForAWSEle.isDisplayed() === true;
        }, {timeout: CONSTANT.GENERAL_WAIT});

        elementUtil.clickOnElement(splunkAppForAWSEle);
    }
}

module.exports = new HomePage();