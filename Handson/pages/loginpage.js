const elementUtil = require('../util/elementutil');

class LoginPage {
   
    get userNameTextBox() {
        return $('#username');
    }

    get passwordTextBox() {
        return $('#password');
    }

    get signInButton() {
        return $('//input[@type="submit" and @value="Sign In"]');
    }

    enterText(element, text) {
        element.waitForDisplayed();
        element.setValue(text);
    }

    login(username, password) {

        browser.url("http://127.0.0.1:8000/en-US/account/login");
        browser.maximizeWindow();

        const userNameField = this.userNameTextBox;
        const passwordField = this.passwordTextBox;
        const signInButton = this.signInButton;

        this.enterText(userNameField, username);
        this.enterText(passwordField, password);

        elementUtil.clickOnElement(signInButton);

    }

}

module.exports = new LoginPage();