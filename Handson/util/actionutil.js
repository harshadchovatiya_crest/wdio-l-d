const displayData = require('../pages/displaydata');
const CONSTANT = require('../constant');
const { assert } = require('chai');


class ActionUtil {

    verifyAllAccountIDAndAllRegion() {
        let spans;
        
        browser.waitUntil(function() {
            spans = displayData.spanOfRegionValue;
            return spans.length > 1;
        }, {timeout: CONSTANT.GENERAL_WAIT});
        
        const span1 = spans[0];
        const span2 = spans[1];
        assert.equal(span1.getText(), '"*"');
        assert.equal(span2.getText(), '"*"');
   
    }

}

module.exports = new ActionUtil();