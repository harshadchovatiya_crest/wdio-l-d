class ElementUtil {
    clickOnElement(element) {
        element.waitForDisplayed();
        element.click();
    }

    fetchText(element) {
        element.waitForDisplayed();
        return element.getText();
    }
}

module.exports = new ElementUtil();