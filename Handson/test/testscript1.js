const elementUtil = require('../util/elementutil');
const loginPage = require('../pages/loginpage');
const homePage = require('../pages/homepage');
const EBSVolume = require('../pages/ebsvolume');
const CONSTANT = require('../constant');
const displayData = require('../pages/displaydata');
const actionUtil = require('../util/actionutil');
const splunkAppAws = require('../pages/splunkappaws');
const { assert } = require('chai');

describe("test case for multiple account ids", function() {

    before(function() {

        loginPage.login("admin", "admin123");

    });

    it("navigate to splunk app for aws", function() {

        homePage.navigateToSplunkAppForAWS();

    });

    it("click on EBS volume", function() {

        splunkAppAws.clickOnEBSVolumes();

    });

    // it("click on account id", function() {

    //     EBSVolume.clickOnAccountIDField();

    // });

    // it("clear account ids", function() {

    //     EBSVolume.clearValueOfAccountIDField();

    // });

    // it("select account id1", function() {

    //     EBSVolume.selectAccountIDUsingIndex(1, 3);

    // });


    // it("select account id2", function() {

    //     EBSVolume.selectAccountIDUsingIndex(1,2);

    // });

    // it("verify selected account ids", function() {

    //     let selectedButtons;

    //     browser.waitUntil(function() {
    //         selectedButtons = EBSVolume.selectedButtonOfAccountField;
    //         return selectedButtons.length === 2;
    //     }, {timeout:5000});

    //     assert.equal(selectedButtons[0].getText().includes(CONSTANT.BUTTON1_TEXT), true);
    //     assert.equal(selectedButtons[1].getText().includes(CONSTANT.BUTTON2_TEXT), true);
        
    
    // });

    // it("click on data and verify account ids", function() {

    //     EBSVolume.clickOnPanel("in-use-ebs-volumes")

    //     browser.switchWindow('Search');

    //     const spans = displayData.getSpansOfSelectedValues();
    //     const span1 = spans[0]; 
    //     assert.equal(span1.getText(), '"' + CONSTANT.ACCOUNT_ID_1 + '"');

    //     const span2 = spans[1];
    //     assert.equal(span2.getText(), '"' + CONSTANT.ACCOUNT_ID_2 + '"');

    // });

});