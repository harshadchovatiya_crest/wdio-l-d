const elementUtil = require('../util/elementutil');
const loginPage = require('../pages/loginpage');
const homePage = require('../pages/homepage');
const EBSVolume = require('../pages/ebsvolume');
const CONSTANT = require('../constant');
const displayData = require('../pages/displaydata');
const actionUtil = require('../util/actionutil');
const splunkAppAws = require('../pages/splunkappaws');
const { assert } = require('chai');

describe("test scenario with single region", function() {

    before(function() {

        loginPage.login("admin", "admin123");

    });

    it("navigate to splunk app for aws", function() {

        homePage.navigateToSplunkAppForAWS();

    });

    it("click on EBS volume", function() {

        splunkAppAws.clickOnEBSVolumes();

    });

    it("click on region field", function() {

        EBSVolume.clickOnRegionField();

    });

    it("clear value of region field", function() {

        EBSVolume.clearValueOfRegionField();

    });


    it("select value of region", function() {

        EBSVolume.selectRegionUsingIndex(2);

    });

    it("click on data", function() {

        EBSVolume.clickOnPanel("in-use-ebs-volume-size");
        
    });

    it("verify regions", function() {

        browser.switchWindow('Search');

        const spans = displayData.getSpansOfSelectedValues(); 
        const span = spans[1];

        assert.equal(span.getText(), '"ap-northeast-2"');

    });

});