const elementUtil = require('../util/elementutil');
const loginPage = require('../pages/loginpage');
const homePage = require('../pages/homepage');
const EBSVolume = require('../pages/ebsvolume');
const CONSTANT = require('../constant');
const displayData = require('../pages/displaydata');
const actionUtil = require('../util/actionutil');
const splunkAppAws = require('../pages/splunkappaws');
const { assert } = require('chai');

describe("test in use ebs volume size panel", function() {

    before(function() {

        loginPage.login("admin", "admin123");

    });

    it("navigate to splunk app for aws", function() {

        homePage.navigateToSplunkAppForAWS();

    });

    it("click on EBS volume", function() {

        splunkAppAws.clickOnEBSVolumes();

    });

    it("check in use ebs volume size panel is present", function() {

        EBSVolume.checkPresenceOfInUseEBSVolumeSizePanel();

    });

    // it("check panel should be of type single value", function() {

    //     let outOf;
    //     browser.waitUntil(function() {
    //        outOf = EBSVolume.inUseEBSVolumeSizeTotalOf;
    //        return outOf.isExisting() === true
    //     }, {timeout: 45000});
        
    //     // verify panel should be of type single value
    //     assert.equal(outOf.getAttribute('class'), 'single-total')

    // });


    it("check for no alert is present", function() {

        EBSVolume.checkForAlertSymbolTemp("in-use-ebs-volume-size");

    });

    it("check for no warning is present", function() {

        EBSVolume.checkForWarningSymbolTemp("in-use-ebs-volume-size");

    });

});