const elementUtil = require('../util/elementutil');
const loginPage = require('../pages/loginpage');
const homePage = require('../pages/homepage');
const EBSVolume = require('../pages/ebsvolume');
const CONSTANT = require('../constant');
const displayData = require('../pages/displaydata');
const actionUtil = require('../util/actionutil');
const splunkAppAws = require('../pages/splunkappaws');
const { assert } = require('chai');


describe("test scenario with two regions", function() {

    before(function() {

        loginPage.login("admin", "admin123");

    });

    it("navigate to splunk app for aws", function() {

        homePage.navigateToSplunkAppForAWS();

    });

    it("click on EBS volume", function() {

        splunkAppAws.clickOnEBSVolumes();

    });

    it("click on region field", function() {

        EBSVolume.clickOnRegionField();

    });

    it("clear value of region field", function() {

        EBSVolume.clearValueOfRegionField();

    });


    it("select value of region 1", function() {

        EBSVolume.selectRegionUsingIndex(1);

    });

    it("select value of region 2", function() {

        EBSVolume.selectRegionUsingIndex(1);
        
    });


    it("click on data", function() {

        EBSVolume.clickOnPanel("in-use-ebs-volume-size");
       
    });

    it("verify regions", function() {

        browser.switchWindow('Search');

        const spans = displayData.getSpansOfSelectedValues(); 
        
        const span1 = spans[1];
        const span2 = spans[2];
        assert.equal(span1.getText(), '"ap-northeast-1"');
        assert.equal(span2.getText(), '"ap-northeast-2"');

    });

});