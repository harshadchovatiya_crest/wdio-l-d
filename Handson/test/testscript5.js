const elementUtil = require('../util/elementutil');
const loginPage = require('../pages/loginpage');
const homePage = require('../pages/homepage');
const EBSVolume = require('../pages/ebsvolume');
const CONSTANT = require('../constant');
const displayData = require('../pages/displaydata');
const actionUtil = require('../util/actionutil');
const splunkAppAws = require('../pages/splunkappaws');
const { assert } = require('chai');

describe("test in use ebs volumes panel", function() {

    before(function() {

        loginPage.login("admin", "admin123");

    });

    it("navigate to splunk app for aws", function() {

        homePage.navigateToSplunkAppForAWS();

    });

    it("click on EBS volume", function() {

        splunkAppAws.clickOnEBSVolumes();

    });

    it("check in use ebs volume panel is present", function() {

        EBSVolume.checkPresenceOfInUseEBSVolumesPanel();

    });

    // it("check panel should be of type single value and contains out of text", function() {

    //     let outOf;
    //     browser.waitUntil(function() {
    //        outOf = EBSVolume.inUseEBSVolumesTotalOf;
    //        return outOf.isDisplayed() === true
    //     }, {timeout: 45000});
    //     assert.equal(outOf.getText().includes("out of"), true);
        
    //     // verify panel should be of type single value
    //     assert.equal(outOf.getAttribute('class'), 'single-total')

    // });


    it("check for no alert is present on in use ebs volumes panel", function() {

        EBSVolume.checkForAlertSymbolTemp("in-use-ebs-volumes");

    });

    it("check for no warning is present", function() {

        EBSVolume.checkForWarningSymbolTemp("in-use-ebs-volumes");

    });
});