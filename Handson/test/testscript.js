const loginPage = require('../pages/loginpage');
const homePage = require('../pages/homepage');
const EBSVolume = require('../pages/ebsvolume');
const CONSTANT = require('../constant');
const displayData = require('../pages/displaydata');
const actionUtil = require('../util/actionutil');
const splunkAppAws = require('../pages/splunkappaws');
const { assert } = require('chai');

describe("test splunk app for aws", function() {

    // run only once before starting of the test cases
    before(function() {

        loginPage.login(CONSTANT.USERNAME, CONSTANT.PASSWORD);

    });

    it("navigate to splunk app for aws", function() {

        homePage.navigateToSplunkAppForAWS();

    });

    it("click on EBS volume", function() {

        splunkAppAws.clickOnEBSVolumes();

    });

    it("click on account id", function() {

        EBSVolume.clickOnAccountIDField();

    });

    it("clear account ids", function() {

        EBSVolume.clearValueOfAccountIDField();

    });

    it("select account id1", function() {

        EBSVolume.selectAccountIDUsingIndex(1, 3);

    });


    it("select account id2", function() {

        EBSVolume.selectAccountIDUsingIndex(1, 2);

    });

    it("verify selected account ids", function() {

        let selectedButtons;
        browser.waitUntil(function() {
            selectedButtons = EBSVolume.selectedButtonOfAccountField;
            return selectedButtons.length === 2;
        }, {timeout: CONSTANT.GENERAL_WAIT});

        assert.equal(selectedButtons[0].getText().includes(CONSTANT.BUTTON1_TEXT), true);
        assert.equal(selectedButtons[1].getText().includes(CONSTANT.BUTTON2_TEXT), true);
        
    
    });

    it("click on data and verify account ids in search tab", function() {

        EBSVolume.clickOnPanel("in-use-ebs-volumes")

        browser.switchWindow('Search');
        
        const spans = displayData.getSpansOfSelectedValues();

        const span1 = spans[0]; 
        assert.equal(span1.getText(), '"' + CONSTANT.ACCOUNT_ID_1 + '"');

        const span2 = spans[1];
        assert.equal(span2.getText(), '"' + CONSTANT.ACCOUNT_ID_2 + '"');

        browser.closeWindow();
        browser.switchWindow("EBS Volumes")

    });

    it("click on account id", function() {

        EBSVolume.clickOnAccountIDField();

    });

    it("clear account ids", function() {

        EBSVolume.clearValueOfAccountIDField();

    });

    it("select all option in account id", function() {

        EBSVolume.selectAccountIDUsingIndex(0, 3);

    });

    it("click on region field", function() {

        EBSVolume.clickOnRegionField();

    });

    it("clear value of region field", function() {

        EBSVolume.clearValueOfRegionField();

    });

    it("enter all option in region field", function() {
   
        EBSVolume.selectRegionUsingIndex(0);
    
    });


    it("click on data", function() {

        EBSVolume.clickOnPanel("in-use-ebs-volume-size");
        
    });

    it("verify regions", function() {

        browser.switchWindow('Search');

        const spans = displayData.getSpansOfSelectedValues(); 
        
        const span = spans[1];

        assert.equal(span.getText(), '"*"');

        browser.closeWindow();
        browser.switchWindow("EBS Volumes")

    });

    it("click on region field", function() {

        EBSVolume.clickOnRegionField();

    });

    it("clear value of region field", function() {

        EBSVolume.clearValueOfRegionField();

    });


    it("select value of region", function() {

        EBSVolume.selectRegionUsingIndex(2);

    });

    it("click on data", function() {

        EBSVolume.clickOnPanel("in-use-ebs-volume-size");
        
    });

    it("verify regions", function() {

        browser.switchWindow('Search');

        const spans = displayData.getSpansOfSelectedValues(); 
        
        const span = spans[1];

        assert.equal(span.getText(), '"ap-northeast-2"');

        browser.closeWindow();
        browser.switchWindow("EBS Volumes")

    });

    it("click on region field", function() {

        EBSVolume.clickOnRegionField();

    });

    it("clear value of region field", function() {

        EBSVolume.clearValueOfRegionField();

    });


    it("select value of region 1", function() {

        EBSVolume.selectRegionUsingIndex(1);

    });

    it("select value of region 2", function() {

        EBSVolume.selectRegionUsingIndex(1);

    });


    it("click on data", function() {

        EBSVolume.clickOnPanel("in-use-ebs-volume-size");
       
    });

    it("verify regions", function() {

        browser.switchWindow('Search');

        const spans = displayData.getSpansOfSelectedValues(); 

        const span1 = spans[1];
        const span2 = spans[2];
        assert.equal(span1.getText(), '"ap-northeast-1"');
        assert.equal(span2.getText(), '"ap-northeast-2"');

        browser.closeWindow();
        browser.switchWindow("EBS Volumes")
    });

    it("click on region field", function() {

        EBSVolume.clickOnRegionField();

    });

    it("clear value of region field", function() {

        EBSVolume.clearValueOfRegionField();

    });

    it("enter all in region field", function() {

        EBSVolume.selectRegionUsingIndex(0);

    });

    it("check in use ebs volume panel is present", function() {

        EBSVolume.checkPresenceOfInUseEBSVolumesPanel();

    });

    it("check panel should be of type single value and contains out of text", function() {

        let outOf;
        browser.waitUntil(function() {
           outOf = EBSVolume.inUseEBSVolumesTotalOf;
           return outOf.isDisplayed() === true
        }, {timeout: CONSTANT.PANEL_DATA_WAIT});
        assert.equal(outOf.getText().includes("out of"), true);
        
        // verify panel should be of type single value
        assert.equal(outOf.getAttribute('class'), 'single-total')

    });


    it("check for no alert is present in panel of in use ebs volumes", function() {

        EBSVolume.checkForAlertSymbolInPanel("in-use-ebs-volumes");

    });

    it("check for no warning is present in panel of in use ebs volumes", function() {

        EBSVolume.checkForWarningSymbolInPanel("in-use-ebs-volumes");

    });

    it("check in use ebs volume panel is present", function() {

        EBSVolume.checkPresenceOfInUseEBSVolumesPanel();

    });

    it("click on data", function() {

        EBSVolume.clickOnPanel("in-use-ebs-volumes");
    });


    it("check for redirect to search", function() {

        browser.switchWindow('Search');
        assert.equal(browser.getTitle().includes('Search'), true);

    });

    it("check accountid and region selected to all", function() {

        actionUtil.verifyAllAccountIDAndAllRegion();
        
        browser.closeWindow();
        browser.switchWindow("EBS Volumes")

    });

    it("check in use ebs volume size panel is present", function() {

        EBSVolume.checkPresenceOfInUseEBSVolumeSizePanel();

    });

    it("check panel should be of type single value", function() {

        let outOf;
        browser.waitUntil(function() {
           outOf = EBSVolume.inUseEBSVolumeSizeTotalOf;
           return outOf.isExisting() === true
        }, {timeout: CONSTANT.PANEL_DATA_WAIT});
        
        // verify panel should be of type single value
        assert.equal(outOf.getAttribute('class'), 'single-total')

    });


    it("check for no alert is present in panel of in use ebs volume size", function() {

        EBSVolume.checkForAlertSymbolInPanel("in-use-ebs-volume-size");

    });

    it("check for no warning is present in panel of in use ebs volume size", function() {

        EBSVolume.checkForWarningSymbolInPanel("in-use-ebs-volume-size");

    });

    it("check in use ebs volume size panel is present", function() {

        EBSVolume.checkPresenceOfInUseEBSVolumeSizePanel();

    });

    it("click on data", function() {

        EBSVolume.clickOnPanel("in-use-ebs-volume-size");

    });


    it("check for redirect to search", function() {

        browser.switchWindow('Search');
        assert.equal(browser.getTitle().includes('Search'), true);

    });

    it("check accountid and region selected to all", function() {

        actionUtil.verifyAllAccountIDAndAllRegion();

    });

});